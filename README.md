# How to use:
----
###### Save the DocToPDF.ps1 file in the working directory from where all the docx file are saved and simply run the script by opening powershell in the current directory and enter command.

*  > ./DocToPDF.ps1
#
###### Alternatively you can also specify the directory path of docx file. Open powershell where DocToPDF.ps1 is saved and enter command 
* >  ./DocToPDF.ps1 "/path/to/doc/or/ppt/folder"  

###### The script is commented in the If-Else block which specifies the function calls corresponding the different file formats. 
###### For conversion of pptx or excel or publisher to pdf, simply uncomment the functions(Wrd-Chk, Exl-Chk, Pub-Chk) from if-else block.
#
-----
#
#### There are two ps1 files( use any one ) -
#
- DocToPDF.ps1  can be used for converting all the pptx, excel, word and publisher files to pdf.
-------
  The path for the documents can be provided by an argument in powershell
  The steps to be followed are:
>  Start powershell
    navigate to the directory where the convert2.ps1 script is saved.
    #
    type:
> >    DocToPDF.ps1 "document path"
######              ex: 
            DocToPDF.ps1 "C:\Users\adity\Downloads\Test_Folder\ppt_files"
  The pdf will be stored in the location of document files. Once it gets converted then one could also add a Remove-Item command in the functions.
  Here I have written the remove-item command in pow-PDF function.

  The file directory once mentioned will recursively call all the documents and convert it into pdf.
#
-  Convert.ps1  can be used only for converting pptx to pdf.
-----
  The variable $pptPath specifies the directory of powerpoint files. 
  After each succesfull conversion to pdf the files gets removed.
  The commnad  Remove-Item -path $pfile deletes the respective file.
 
# NOTE 
> The conversion uses Microsoft office API, So it is mandatory that you should have a genuine or cracked office 10 or above.
    