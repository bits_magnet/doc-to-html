﻿# Add the PowerPoint assemblies that we'll need
Add-type -AssemblyName office -ErrorAction SilentlyContinue
Add-Type -AssemblyName microsoft.office.interop.powerpoint -ErrorAction SilentlyContinue

# Start PowerPoint
$ppt = new-object -com powerpoint.application
$ppt.visible = [Microsoft.Office.Core.MsoTriState]::msoTrue

# Set the locations where to find the PowerPoint files, and where to store the thumbnails
$pptPath = "C:\Users\adity\Downloads\Test_Folder\abc\"


# Loop through each PowerPoint File
Foreach($iFile in $(ls $pptPath -Filter "*.ppt")){
Set-ItemProperty ($pptPath + $iFile) -name IsReadOnly -value $false
$filename = Split-Path $iFile -leaf
$file = $filename.Split(".")[0]
$oFile = $pptPath + $file + ".pdf" 

# Open the PowerPoint file
$pres = $ppt.Presentations.Open($pptPath + $iFile)

# Now save it away as PDF 
$opt= [Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType]::ppSaveAsPDF
#echo $opt $ofile $iFile
$pres.SaveAs($ofile,$opt)
$pFile = $pptPath + $file + ".pptx"
echo $pFile

# and Tidy-up 
$pres.Close();
Remove-Item -path $pFile

}

#Clean Up
$ppt.quit();
$ppt = $null
[gc]::Collect();
[gc]::WaitForPendingFinalizers();